package ru.oooinex.booktrain;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.sql.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * dbogdanov, 10.09.2018
 */
public class BuyAllTickets {

  private static final String URL = "jdbc:oracle:thin:@172.22.2.10:1521:BAZA";
  private static final String USER = "cwp_common";
  private static final String PASSWORD = "cwp_common";

  private static final String STATION_FROM = "2000000";
  private static final String STATION_TO = "2004000";
  private static final String DEPARTURE_DATE = "01.10.2018";
  private static final String DEPARTURE_TIME = "6:50";
  private static final String TRAIN_NUMBER = "754А";

//  private static final String[] CAR_NUMBERS = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
  private static final String[] CAR_NUMBERS = {"01"};
//    private static final String[] CAR_CLASSES = {"2С", "2В", "2Е", "1С", "1В", "1Р"};
  private static final String[] CAR_CLASSES = {"1В"};

  private static final String WITH_ANIMAL = "<Requirements><SeatsArea>Ж</SeatsArea></Requirements>";
  private static final String DISABLED = "<Requirements><SeatsArea>И</SeatsArea><Disabled /></Requirements>";
  private static final String ADDITIONAL_INFO_WITH_KIDS = "  <Requirements><SeatsArea>Р</SeatsArea></Requirements>";
  private static final String ADDITIONAL_INFO_WITH_BABY = "  <Requirements><SeatsArea>М</SeatsArea></Requirements>";

  private static final String WITH_KIDS = "" +
      "<Blank>" +
      "  <SeatsCount>1</SeatsCount>" +
      "  <Tariff>/ДЕТ5/20</Tariff>" +
      "  <Passenger>" +
      "    <DocType>СР</DocType>" +
      "    <Doc>IVБЮ123456</Doc>" +
      "    <Name>КУЧИНСКАЯ=ЭДУАРД=АЛЕКСЕЕВИЧ</Name>" +
      "    <Birthday>06.02.2018</Birthday>" +
      "    <Birthplace />" +
      "    <Citizenship>RUS</Citizenship>" +
      "    <Sex>F</Sex>" +
      "  </Passenger>" +
      "</Blank>";

  private static final String BOOK_REQUEST = "<GtExpress_Request Type=\"BuyTicket\">" +
      "  <StationFrom>" + STATION_FROM + "</StationFrom>" +
      "  <StationTo>" + STATION_TO + "</StationTo>" +
      "  <DepDate>" + DEPARTURE_DATE + "</DepDate>" +
      "  <DepTime>" + DEPARTURE_TIME + "</DepTime>" +
      "  <Train Number=\"" + TRAIN_NUMBER + "\" />" +
      "  <Car Type=\"Сид\" Number=\"%s\" Carrier=\"ДОСС\" ClassService=\"%s\" />" +
      "  %s" +
      "  <CreditCard />" +
      "  <Passengers>" +
      "    <Blank>" +
      "      <SeatsCount>1</SeatsCount>" +
      "      <Passenger>" +
      "        <DocType>ПН</DocType>" +
      "        <Doc>4602123321</Doc>" +
      "        <Name>ИВАНОВ=ИВАН=ИВАНОВИЧ</Name>" +
      "        <Birthday>03.09.1989</Birthday>" +
      "        <Birthplace />" +
      "        <Citizenship>RUS</Citizenship>" +
      "        <Sex>M</Sex>" +
      "      </Passenger>" +
      "    </Blank>" +
      "    %s" +
      "  </Passengers>" +
      "</GtExpress_Request>";

  private static final String CONFIRM_REQUEST = "" +
      "<GtExpress_Request Type=\"Registration\" User=\"Inex\" Priority=\"2\">\n" +
      "  <Order ExpressID=\"%s\" State=\"0\" />\n" +
      "  <UserID>dbogdanov</UserID>\n" +
      "  <TransID>%s</TransID>\n" +
      "  <TradingPlace>0001</TradingPlace>\n" +
      "  <PayAgent>2001</PayAgent>\n" +
      "</GtExpress_Request>";

  public static void main(String[] args) throws Exception {

    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

    Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);

    CloseableHttpClient client = HttpClients.createDefault();
    HttpPost httpPost = new HttpPost("http://test:test@10.240.5.116:19080/test");

    for (String carClass : CAR_CLASSES) {
      for (String carNumber : CAR_NUMBERS) {
        // бронируем обычные места
        buyAllTickets(connection, client, httpPost, carClass, carNumber, "", "");

        // бронируем места с животными
        buyAllTickets(connection, client, httpPost, carClass, carNumber, WITH_ANIMAL, "");

        // бронируем места для инвалидов
        buyAllTickets(connection, client, httpPost, carClass, carNumber, DISABLED, "");

        // бронируем места для пассажиров с детьми
        buyAllTickets(connection, client, httpPost, carClass, carNumber, ADDITIONAL_INFO_WITH_KIDS, WITH_KIDS);

        // бронируем места для пассажиров с детьми
        buyAllTickets(connection, client, httpPost, carClass, carNumber, ADDITIONAL_INFO_WITH_BABY, WITH_KIDS);
      }
    }
  }

  private static void buyAllTickets(Connection connection, CloseableHttpClient client, HttpPost httpPost, String carClass, String carNumber, String additionalInfo, String withKids) throws Exception {
    while (true) {

      String bookRequest = String.format(BOOK_REQUEST, carNumber, carClass, additionalInfo, withKids);
//      System.out.println(bookRequest);
      httpPost.setEntity(new ByteArrayEntity(bookRequest.getBytes("UTF-8")));

      HttpResponse response = client.execute(httpPost);
      String result = EntityUtils.toString(response.getEntity(), "UTF-8");
//      System.out.println(result);
      if (result.contains("Error")) {
        return;
      }

//      System.out.println(result);
      String expressId = result.substring(result.indexOf("<Order ExpressID=") + 18, result.indexOf("<Order ExpressID=") + 32);
//      System.out.println(expressId);
      String transactionId = generateTransactionId(connection, new Date());
//      System.out.println(expressId);

      String confirmRequest = String.format(CONFIRM_REQUEST, expressId, transactionId);
      httpPost.setEntity(new ByteArrayEntity(confirmRequest.getBytes("UTF-8")));

      response = client.execute(httpPost);
      result = EntityUtils.toString(response.getEntity(), "UTF-8");
//      System.out.println(result);
    }
  }

  private static String generateTransactionId(Connection conn, Date issueDate) throws SQLException {

    // Подготовим данные для вычисления префикса ID транзакции, зависящего от даты оформления заказа
    Calendar calendar = GregorianCalendar.getInstance();
    calendar.setTime(issueDate);
    int day = calendar.get(Calendar.DAY_OF_YEAR);
    int year = calendar.get(Calendar.YEAR);
    int dateId = (day / 2) * 5 + year % 5;

    // Вычислим префикс ID транзакции, зависящий от даты оформления заказа
    String prefix = generateId("8", dateId, 4);

    // Получим очередной порядковый номер из заданного диапазона, составляющий втроую часть ID транзакции
    String transactionIdBody = getTransactionIdBody(conn);

    // Получаем и возвращаем ID транзакции
    return addCheckDigit(prefix + transactionIdBody);
  }

  private static String generateId(String prefix, long id, int length) {
    String idString = String.valueOf(id);
    int leadingZeroCount = length - prefix.length() - idString.length();
    StringBuilder leadingZeroes = new StringBuilder();
    for (int i = 0; i < leadingZeroCount; i++) {
      leadingZeroes.append("0");
    }
    return prefix + leadingZeroes.toString() + idString;
  }

  private static String getTransactionIdBody(Connection conn) throws SQLException {
    CallableStatement cs = conn.prepareCall("{? = call cwp_sys.get_part_transaction_id()}");
    cs.registerOutParameter(1, Types.VARCHAR);
    cs.execute();
    return cs.getString(1);
  }

  private static int computeCheckDigit(String number) {
    int sum = 0;
    boolean alternate = true;
    for (int i = number.length() - 1; i >= 0; i--) {
      int digit = Integer.parseInt(number.substring(i, i + 1));
      if (alternate) {
        digit *= 2;
        if (digit > 9) {
          digit = (digit % 10) + 1;
        }
      }
      sum += digit;
      alternate = !alternate;
    }
    return (sum * 9) % 10;
  }

  private static String addCheckDigit(String number) {
    return number + computeCheckDigit(number);
  }
}
