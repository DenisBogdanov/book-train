package ru.oooinex.booktrain;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * dbogdanov, 10.09.2018
 */
public class BookAllCars {

  private static final String STATION_FROM = "2000000";
  private static final String STATION_TO = "2004000";
  private static final String DEPARTURE_DATE = "21.09.2018";
  private static final String DEPARTURE_TIME = "05:45";
  private static final String TRAIN_NUMBER = "752А";

  private static final String[] CAR_NUMBERS = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10"};
  private static final String[] CAR_CLASSES = {"2С", "2В", "2Е", "1С", "1В", "1Р"};

  private static final String WITH_ANIMAL = "<Requirements><SeatsArea>Ж</SeatsArea></Requirements>";
  private static final String DISABLED = "<Requirements><SeatsArea>И</SeatsArea><Disabled /></Requirements>";
  private static final String ADDITIONAL_INFO_WITH_KIDS = "  <Requirements><SeatsArea>Р</SeatsArea></Requirements>";

  private static final String WITH_KIDS = "<Blank>" +
      "      <SeatsCount>1</SeatsCount>" +
      "      <Tariff>/ДЕТ5/20</Tariff>" +
      "      <Passenger>" +
      "        <DocType>СР</DocType>" +
      "        <Doc>IVБЮ123456</Doc>" +
      "        <Name>КУЧИНСКАЯ=ЭДУАРД=АЛЕКСЕЕВИЧ</Name>" +
      "        <Birthday>06.02.2018</Birthday>" +
      "        <Birthplace />" +
      "        <Citizenship>RUS</Citizenship>" +
      "        <Sex>F</Sex>" +
      "      </Passenger>" +
      "    </Blank>";

  private static final String BOOK_REQUEST = "<GtExpress_Request Type=\"BuyTicket\">" +
      "  <StationFrom>" + STATION_FROM + "</StationFrom>" +
      "  <StationTo>" + STATION_TO + "</StationTo>" +
      "  <DepDate>" + DEPARTURE_DATE + "</DepDate>" +
      "  <DepTime>" + DEPARTURE_TIME + "</DepTime>" +
      "  <Train Number=\"" + TRAIN_NUMBER + "\" />" +
      "  <Car Type=\"Сид\" Number=\"%s\" Carrier=\"ДОСС\" ClassService=\"%s\" />" +
      "  %s" +
      "  <CreditCard />" +
      "  <Passengers>" +
      "    <Blank>" +
      "      <SeatsCount>1</SeatsCount>" +
      "      <Passenger>" +
      "        <DocType>ПН</DocType>" +
      "        <Doc>4602123321</Doc>" +
      "        <Name>ИВАНОВ=ИВАН=ИВАНОВИЧ</Name>" +
      "        <Birthday>03.09.1989</Birthday>" +
      "        <Birthplace />" +
      "        <Citizenship>RUS</Citizenship>" +
      "        <Sex>M</Sex>" +
      "      </Passenger>" +
      "    </Blank>" +
      "    %s" +
      "  </Passengers>" +
      "</GtExpress_Request>";

  public static void main(String[] args) throws Exception {

    CloseableHttpClient client = HttpClients.createDefault();
    HttpPost httpPost = new HttpPost("http://test:test@10.240.5.116:19080/test");

    for (String carClass : CAR_CLASSES) {
      for (String carNumber : CAR_NUMBERS) {
        // бронируем обычные места
        bookAllCars(client, httpPost, carClass, carNumber, "", "");

        // бронируем места с животными
        bookAllCars(client, httpPost, carClass, carNumber, WITH_ANIMAL, "");

        // бронируем места для инвалидов
        bookAllCars(client, httpPost, carClass, carNumber, DISABLED, "");

        // бронируем места для пассажиров с детьми
        bookAllCars(client, httpPost, carClass, carNumber, ADDITIONAL_INFO_WITH_KIDS, WITH_KIDS);
      }
    }
  }

  private static void bookAllCars(CloseableHttpClient client, HttpPost httpPost, String carClass, String carNumber, String additionalInfo, String withKids) throws IOException {
    while (true) {

      String bookRequest = String.format(BOOK_REQUEST, carNumber, carClass, additionalInfo, withKids);
      httpPost.setEntity(new ByteArrayEntity(bookRequest.getBytes("UTF-8")));

      HttpResponse response = client.execute(httpPost);
      String result = EntityUtils.toString(response.getEntity(), "UTF-8");
      if (result.contains("Error")) {
        break;
      }
    }
  }
}
